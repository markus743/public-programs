#!/bin/bash
# FILE:      /home/users/MyUsername/.bashrc
# FUNCTION:  This file is automatically called at start-up on a Unix machine

# activate bash
# /bin/bash

# Source global definitions
if [ -f /etc/bash.bashrc ]; then
       . /etc/bash.bashrc
fi

umask 002

# Bold (01) orange (33) for directories
# Bold (01) yellow (93) for files
export LS_COLORS="$LS_COLORS:di=01;33:fi=01;93"

function setprmt()
  {
    PS1='ServerX:$PWD>\n $ '
  }
setprmt

function n()
  {
    nohup bash $1 > ../log/$1.log 2> ../log/$1.err < /dev/null &
  }

function e()
  {
    vim -Nu ~/.vim/.vimrc $1
  }

function s_front {
   WD=$(pwd)                        # Working directory
   PROJECT_DIR="${WD%/program}"     # remove /program from $WD
   export WD
   export PROJECT_DIR
   SAS_DIR="/opt/sas/home/SASFoundation/9.4/sas"
   PROG_DIR="${WD%/*}/program"
   LOG_DIR="${WD%/*}/log"
   OUT_DIR="${WD%/*}/output"
   PROG=$1
   export PROG
   echo "$PROG_DIR/$1"
   $SAS_DIR -noterminal -log $LOG_DIR -print $OUT_DIR $PROG_DIR/$1 
}

function s {
   WD=$(pwd)                        # Working directory
   PROJECT_DIR="${WD%/program}"     # remove /programfrom $WD
   export WD
   export PROJECT_DIR
   SAS_DIR="/opt/sas/home/SASFoundation/9.4/sas"
   PROG_DIR="${WD%/*}/program"
   LOG_DIR="${WD%/*}/log"
   OUT_DIR="${WD%/*}/output"
   PROG=$1
   export PROG
   echo "$PROG_DIR/$1"
#  $SAS_DIR -noterminal -nonews -log $LOG_DIR -print $OUT_DIR -work /data/saswork $PROG_DIR/$1 -set SAS_NON_XVIEW_TABLES YES &
   $SAS_DIR -noterminal -nonews -log $LOG_DIR -print $OUT_DIR -work /data/saswork $PROG_DIR/$1 &}

function bt {
   WD=$(pwd)                        # Working directory
   PROJECT_DIR="${WD%/program}"     # remove /programfrom $WD
   export WD
   export PROJECT_DIR
   PROG_DIR="${WD%/*}/program"
   LOG_DIR="${WD%/*}/log"
   OUT_DIR="${WD%/*}/output"
   BTEQ=$1
   export BTEQ
   echo "$PROG_DIR/$1"
   bteq < $PROG_DIR/$1 > $LOG_DIR/$1.log 2> $LOG_DIR/$1.err &
}

# Create work folder structure
function cw()
  {  mkdir function
     mkdir input
     mkdir log
     mkdir output
     mkdir program
     chmod u=rwx,g=rx,o=rx * 

     cp /data/home/users/MyUsername/.pw/.gitignore input/.gitignore
     chmod u=rw,g=r,o=r input/.gitignore

     cd program
     mkdir sas
     mkdir retired
     chmod u=rwx,g=rx,o=rx * 

     cd ..
     cd output
     mkdir keep
     mkdir qc
     chmod u=rwx,g=rx,o=rx *

     cd ..
     cd log
     mkdir keep
     chmod u=rwx,g=rx,o=rx *

     cd ..
  }

function cd1()
  {
     cd ..
  }
  
function cd2()
  {
     cd ..
     cd ..
  }

function cd3()
  {
     cd ..
     cd ..
     cd ..
  }

function cd4()
  {
     cd ..
     cd ..
     cd ..
     cd ..
  }

function cd5()
  {
     cd ..
     cd ..
     cd ..
     cd ..
     cd ..
  }


export LC_ALL="C"

alias ll='ls -lah --group-directories-first'

cd /xyz/MyUsername